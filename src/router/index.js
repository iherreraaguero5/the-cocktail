import Vue from "vue";
import VueRouter from "vue-router";
import Tab1 from "../views/Tab1.vue";
import Tab3 from "../views/Tab3.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Tab1,
  },
  {
    path: "/tab-3",
    name: "tab-3",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Tab3,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
