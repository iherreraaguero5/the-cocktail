export const appRoutes = {
  HOME_PATH: "home",
  CATEGORIES_PATH: "categories",
  APP: "app",
};

export const apiEndpoints = {
  AUTH_SIGNIN_PATH: "createLogin",
};

export const SERVER_URL = "https://api.punkapi.com/v2/";
