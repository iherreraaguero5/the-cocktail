import { list } from "@/services/drinks";
const LIST_DRINKS = "LIST_DRINKS";

const state = {
  drinks: [],
};
const mutations = {
  LIST_DRINKS(state, drinks) {
    state.drinks = drinks;
  },
};
const actions = {
  getDrinkList: ({ commit }, requestData) => {
    if (requestData === undefined) requestData = {};
    return new Promise((resolve, reject) => {
      list(requestData.params, requestData.controller)
        .then((res) => {
          commit(LIST_DRINKS, res.data);
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
