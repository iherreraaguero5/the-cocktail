import Vue from "vue";
import Vuex from "vuex";
import drinks from "@/store/modules/drinks";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { drinks },
});
