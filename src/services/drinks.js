import api from "@/api/axios";

export const list = (options, controller = new AbortController()) => {
  let p = api.request({
    url: "beers",
    method: "get",
    params: options,
    signal: controller.signal,
  });
  return p;
};
